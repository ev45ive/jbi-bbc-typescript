"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.NotFoundException = exports.DomainError = void 0;
class DomainError {
    constructor() {
        this.message = 'Unknown Error';
        this.code = 500;
    }
    toJSON() {
        return { error: { message: this.message } };
    }
}
exports.DomainError = DomainError;
class NotFoundException extends DomainError {
    constructor() {
        super(...arguments);
        this.message = 'Not Found';
        this.code = 404;
    }
}
exports.NotFoundException = NotFoundException;
//# sourceMappingURL=common.js.map