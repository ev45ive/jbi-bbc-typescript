"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BooksService = exports.Book = void 0;
const common_1 = require("./common");
class Book {
    constructor() {
        this.id = '';
        this.name = '';
    }
    static create(payload) {
        const book = new Book();
        book.id = '',
            book.name = payload.name || '';
        return book;
    }
    toJSON() {
        return {
            type: 'book',
            id: this.id,
            name: this.name
        };
    }
}
exports.Book = Book;
class BooksService {
    constructor() {
        this.books = [
            new Book()
        ];
    }
    find() {
        return __awaiter(this, void 0, void 0, function* () { return this.books; });
    }
    findOne(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const book = this.books.find(b => b.id === id);
            if (!book) {
                throw new common_1.NotFoundException();
            }
            return book;
        });
    }
    create(payload) {
        return __awaiter(this, void 0, void 0, function* () {
            const book = Book.create(payload);
            return book;
        });
    }
    update() {
        return __awaiter(this, void 0, void 0, function* () { });
    }
    delete() {
        return __awaiter(this, void 0, void 0, function* () { });
    }
}
exports.BooksService = BooksService;
// setName():this
// Book.build().setName().setGenre()
//# sourceMappingURL=books.js.map