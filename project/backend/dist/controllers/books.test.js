"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const supertest_1 = __importDefault(require("supertest"));
const books_1 = __importDefault(require("./books"));
it("Responds books", function (done) {
    supertest_1.default(books_1.default)
        .get("/")
        .expect(200)
        .expect({
        greeting: "Hello, World!"
    })
        .end(done);
});
//# sourceMappingURL=books.test.js.map