"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// import { Router } from 'express'
const services_1 = require("../services");
const express_promise_router_1 = __importDefault(require("express-promise-router"));
const router = express_promise_router_1.default();
router.get('/', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    // const { q, limit, pages } = (req.query as unknown as GetQueryParams)
    const { q, limit, pages } = req.query;
    const result = yield services_1.booksService.find();
    res.json(result);
}));
router.get('/:id', 
// (req, res, next) => next('router'), // inline middleware
(req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const result = yield services_1.booksService.findOne(req.params.id);
    res.json(result);
}));
router.post('/', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const result = yield services_1.booksService.create(req.body);
    req.body;
    res.json(result);
}));
router.put('/:id', (req, res) => { });
router.delete('/:id', (req, res) => { });
exports.default = router;
// const handler: GetBooks.RequestHandler = async (req, res) => {
//   // const { q, limit, pages } = (req.query as unknown as GetQueryParams)
//   const { q, limit, pages } = req.query
//   const result = await booksService.find()
//   res.json(result)
// }
/*
router.get('/:id', async (req, res, next) => {
  try {
    const result = await booksService.findOne()

    res.json(result)
  } catch (error) {
    // res.status(500).json({ error: { message: error.message } })
    next(error)
  }
})
 */ 
//# sourceMappingURL=books.js.map