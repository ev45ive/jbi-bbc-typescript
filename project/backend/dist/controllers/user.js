"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserController = exports.User = exports.GetUsersQuery = exports.Roles = void 0;
require("reflect-metadata");
const class_transformer_1 = require("class-transformer");
const class_validator_1 = require("class-validator");
const routing_controllers_1 = require("routing-controllers");
var Roles;
(function (Roles) {
    Roles["Admin"] = "admin";
    Roles["User"] = "user";
    Roles["Guest"] = "guest";
})(Roles = exports.Roles || (exports.Roles = {}));
class GetUsersQuery {
}
__decorate([
    class_validator_1.IsPositive(),
    class_validator_1.IsOptional(),
    __metadata("design:type", Number)
], GetUsersQuery.prototype, "limit", void 0);
__decorate([
    class_validator_1.IsAlpha(),
    class_validator_1.IsOptional(),
    __metadata("design:type", String)
], GetUsersQuery.prototype, "city", void 0);
__decorate([
    class_validator_1.IsEnum(Roles),
    class_validator_1.IsOptional(),
    __metadata("design:type", String)
], GetUsersQuery.prototype, "role", void 0);
__decorate([
    class_validator_1.IsBoolean(),
    class_validator_1.IsOptional(),
    __metadata("design:type", Boolean)
], GetUsersQuery.prototype, "isActive", void 0);
exports.GetUsersQuery = GetUsersQuery;
class User {
    // @Type(() => Album) albums: Album[];
    getName() {
        return this.lastName + ' ' + this.firstName;
    }
}
__decorate([
    class_transformer_1.Exclude({ toPlainOnly: true }),
    __metadata("design:type", String)
], User.prototype, "password", void 0);
exports.User = User;
let UserController = class UserController {
    getAll(request, response, query) {
        return [];
    }
    post(user) {
        console.log('saving user ' + user.getName());
        return [];
    }
};
__decorate([
    routing_controllers_1.Get('/'),
    __param(0, routing_controllers_1.Req()), __param(1, routing_controllers_1.Res()), __param(2, routing_controllers_1.QueryParams()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, GetUsersQuery]),
    __metadata("design:returntype", void 0)
], UserController.prototype, "getAll", null);
__decorate([
    routing_controllers_1.Post('/'),
    __param(0, routing_controllers_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [User]),
    __metadata("design:returntype", void 0)
], UserController.prototype, "post", null);
UserController = __decorate([
    routing_controllers_1.JsonController('/users')
], UserController);
exports.UserController = UserController;
//# sourceMappingURL=user.js.map