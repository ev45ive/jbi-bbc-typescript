"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const routing_controllers_1 = require("routing-controllers");
const books_1 = __importDefault(require("./controllers/books"));
const user_1 = require("./controllers/user");
const common_1 = require("./services/common");
const app = express_1.default();
app.use(express_1.default.static('/public'));
app.use(express_1.default.urlencoded());
app.use(express_1.default.json({}));
routing_controllers_1.useExpressServer(app, {
    routePrefix: '/api',
    controllers: [user_1.UserController],
});
// PathParams , ...RequestHandlerParams<P, ResBody, ReqBody, ReqQuery, Locals>
app.get("/", function (req, res) {
    res.status(200).send("<h1>GET request to homepage</h1>");
});
app.use('/books', books_1.default);
const errorhandler = (error, req, res, next) => {
    if (error instanceof common_1.DomainError) {
        return res.status(error.code).json(error);
    }
    res.status(500).json({ error: { message: error.message } });
};
app.use(errorhandler);
const HOST = process.env.HOST || 'localhost';
const PORT = Number(process.env.PORT || 3000);
app.listen(PORT, HOST, () => {
    console.log(`⚡️[server]: Listening on http://${HOST}:${PORT}`);
});
// process.on('uncaughtException', () => {})
// process.on('unhandledRejection', () => {})
//# sourceMappingURL=index.js.map