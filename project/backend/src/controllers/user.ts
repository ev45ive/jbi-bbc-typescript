import 'reflect-metadata'
import { Exclude } from 'class-transformer';
import { IsPositive, IsAlpha, IsEnum, IsBoolean, IsOptional } from 'class-validator';
import { JsonController, Param, Body, Get, Post, Put, Delete, Req, Res, QueryParams } from 'routing-controllers';


export enum Roles {
  Admin = "admin",
  User = "user",
  Guest = "guest",
}

export class GetUsersQuery {

  @IsPositive()
  @IsOptional()
  limit?: number;
  
  @IsAlpha()
  @IsOptional()
  city?: string;
  
  @IsEnum(Roles)
  @IsOptional()
  role?: Roles;
  
  @IsBoolean()
  @IsOptional()
  isActive?: boolean;
}


export class User {
  firstName!: string;
  lastName!: string;

  @Exclude({ toPlainOnly: true }) password!: string;

  // @Type(() => Album) albums: Album[];

  getName(): string {
    return this.lastName + ' ' + this.firstName;
  }
}

@JsonController('/users')
export class UserController {

  @Get('/')
  getAll(@Req() request: any, @Res() response: any, @QueryParams() query: GetUsersQuery) {
    return []
  }

  @Post('/')
  post(@Body() user: User) {
    console.log('saving user ' + user.getName());
    return []
  }
}
