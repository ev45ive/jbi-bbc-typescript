
import supertest from "supertest"
import booksRouter from "./books";


it("Responds books", function (done) {
  supertest(booksRouter)
    .get("/")
    .expect(200)
    .expect({
      greeting: "Hello, World!"
    })
    .end(done);
});