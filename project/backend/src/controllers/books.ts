
// import { Router } from 'express'
import { booksService } from '../services'
import Router from 'express-promise-router'

const router = Router()
// router.use(...)

import { GetBooks, GetBook, CreateBook } from '../interfaces/books'

router.get<
  {},
  GetBooks.ResponseBody,
  {},
  GetBooks.QueryParams
>('/', async (req, res) => {
  // const { q, limit, pages } = (req.query as unknown as GetQueryParams)
  const { q, limit, pages } = req.query

  const result = await booksService.find()

  res.json(result)
})

router.get<GetBook.Params, {}, GetBook.ResponseBody>(
  '/:id',
  // (req, res, next) => next('router'), // inline middleware
  async (req, res) => {
    const result = await booksService.findOne(req.params.id)

    res.json(result)
  })

router.post<
  CreateBook.Params,
  CreateBook.ResponseBody,
  CreateBook.RequestBody
>('/', async (req, res) => {
  const result = await booksService.create(req.body)
  req.body

  res.json(result)
})

router.put('/:id', (req, res) => { })

router.delete('/:id', (req, res) => { })


export default router


// const handler: GetBooks.RequestHandler = async (req, res) => {
//   // const { q, limit, pages } = (req.query as unknown as GetQueryParams)
//   const { q, limit, pages } = req.query

//   const result = await booksService.find()

//   res.json(result)
// }


/*
router.get('/:id', async (req, res, next) => {
  try {
    const result = await booksService.findOne()

    res.json(result)
  } catch (error) {
    // res.status(500).json({ error: { message: error.message } })
    next(error)
  }
})
 */