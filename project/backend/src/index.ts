import express, { ErrorRequestHandler } from "express";
import { useExpressServer } from "routing-controllers";
import booksRouter from "./controllers/books";
import { UserController } from "./controllers/user";
import { DomainError, NotFoundException } from "./services/common";

const app = express();

app.use(express.static('/public'))
app.use(express.urlencoded())
app.use(express.json({}))


useExpressServer(app, {
  routePrefix: '/api',
  controllers: [UserController], // we specify controllers we want to use
});

// PathParams , ...RequestHandlerParams<P, ResBody, ReqBody, ReqQuery, Locals>

app.get("/", function (req, res) {
  res.status(200).send("<h1>GET request to homepage</h1>");
});
app.use('/books', booksRouter)


const errorhandler: ErrorRequestHandler = (error, req, res, next) => {
  if (error instanceof DomainError) {
    return res.status(error.code).json(error)
  }
  res.status(500).json({ error: { message: error.message } })
}
app.use(errorhandler)

const HOST = process.env.HOST || 'localhost';
const PORT = Number(process.env.PORT || 3000)

app.listen(PORT, HOST, () => {
  console.log(`⚡️[server]: Listening on http://${HOST}:${PORT}`)
});

// process.on('uncaughtException', () => {})
// process.on('unhandledRejection', () => {})