import Express, { RequestHandler as ExperssHandler } from "express";
import { Book } from "../services/books";

export interface BookData {
  type: string;
  id: string;
  name: string;
}

export namespace GetBooks {

  export interface QueryParams {
    q: string,
    pages: string,
    limit: string
  }

  export type ResponseBody = Book[]
  export type RequestHandler = Express.RequestHandler<
    {},
    ResponseBody,
    {},
    QueryParams>
}

export namespace GetBook {

  export interface Params {
    id: string
  }

  export type ResponseBody = Book
}

export namespace CreateBook {

  export interface Params { }

  export type ResponseBody = Book
  export type RequestBody = Omit<BookData, 'id' | 'type'>
}