
export class DomainError {
  message = 'Unknown Error'
  code = 500;
  toJSON() {
    return { error: { message: this.message } }
  }
}

export class NotFoundException extends DomainError {
  message = 'Not Found'
  code = 404
}