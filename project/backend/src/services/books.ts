import { BookData } from "../interfaces/books"
import { NotFoundException } from "./common"


export class Book {
  id = ''
  name = ''

  static create(payload: Partial<BookData>) {
    const book = new Book()
    book.id = '',
      book.name = payload.name || ''
    return book;
  }

  toJSON(): BookData {
    return {
      type: 'book',
      id: this.id,
      name: this.name
    }
  }
}

export class BooksService {
  books: Book[] = [
    new Book()
  ]

  async find() { return this.books }

  async findOne(id: string) {
    const book = this.books.find(b => b.id === id)
    if (!book) { throw new NotFoundException() }
    return book
  }

  async create(payload: Omit<BookData, 'id' | 'type'>) {
    const book = Book.create(payload)
    return book;
  }

  async update() { }
  async delete() { }
}


// setName():this
// Book.build().setName().setGenre()