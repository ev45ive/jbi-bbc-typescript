import React from 'react';
import logo from './logo.svg';
import 'bootstrap/dist/css/bootstrap.css';
import { SearchBooks } from './books/pages/SearchBooks';

function App() {
  return (
    <div className="container">
      <div className="row">
        <div className="col">
          <h1 className="">Media App</h1>
          <SearchBooks />
        </div>
      </div>
    </div>
  );
}

// class App {
//   render() {
//     return (
//       <div className="App">
//         <h1 className="">Media App</h1>
//       </div>
//     );
//   }
// }


export default App;
