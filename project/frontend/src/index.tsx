import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';


ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();



// window.React = React;
// window.ReactDOM = ReactDOM;

// var span = (text: string) => React.createElement('span', {}, text)
// var classInfo = 'text-info'

// var div = React.createElement('div', {
//   id: '123',
//   className: classInfo
// },
//   span('Bob and cat'),
//   React.createElement('input', {}),
//   React.createElement('span', {}, 'and monkey'),
// );

// const Span = ({ text }: { text: string }) => <span>{text}</span>

// const App = () => <div id="123" className={classInfo}>
//   <span>Alice has a cat </span>
//   <input />
//   {/* <span>and a dog </span> */}
//   {Span({ text: 'and a dog' })}
//   <Span text=" and a functional components" />
// </div>

// debugger;


// ReactDOM.render(<App />, document.getElementById('root'))

