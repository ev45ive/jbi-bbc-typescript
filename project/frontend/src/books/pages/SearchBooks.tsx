import React, { Reducer, useReducer, useState } from 'react'
import { searchBooks } from '../books-api'
import { SearchBox } from '../components/SearchBox'
import { SearchResults } from '../components/SearchResults'
import { Book } from '../model/Book'

const initialState: State = {
  results: [
    { id: '123', image: 'https://www.placecage.com/c/300/300', name: 'Test 123' },
    { id: '234', image: 'https://www.placecage.com/c/400/400', name: 'Test 234' },
    { id: '345', image: 'https://www.placecage.com/c/500/500', name: 'Test 345' },
    { id: '456', image: 'https://www.placecage.com/c/300/300', name: 'Test 456' },
  ],
  loading: false, query: '', error: ''
}

interface State {
  results: Book[],
  loading: boolean,
  query: string,
  error?: string
}

function makeAction<T>(type: string) {
  const f = (payload: T) => ({ type, ...payload })
  f.type = type;
  return f;
}
const startSearach = makeAction('startSearach')


interface SEARCH_START { type: 'SEARCH_START', payload: { query: string } }
interface SEARCH_SUCCESS { type: 'SEARCH_SUCCESS', payload: { results: Book[] } }
interface SEARCH_FAILED { type: 'SEARCH_FAILED', payload: { error: string } }

type Actions =
  | SEARCH_START
  | SEARCH_SUCCESS
  | SEARCH_FAILED

const reducer: Reducer<State, Actions> = (state = initialState, action) => {

  switch (action.type) {
    case startSearach.type: return state;
    case 'SEARCH_START': return { ...state, loading: true, query: action.payload.query, error: '' };
    case 'SEARCH_SUCCESS': return { ...state, loading: false, results: action.payload.results };
    case 'SEARCH_FAILED': return { ...state, loading: false, error: action.payload.error };
    default:
      return state
  }
}


export const SearchBooks = () => {
  const [{
    results, query, loading, error
  }, dispatch] = useReducer(reducer, initialState)

  const search = async (query: string) => {
    try {

      console.log(query)
      dispatch({ type: 'SEARCH_START', payload: { query } })
      const results = await searchBooks(query)
      dispatch({ type: 'SEARCH_SUCCESS', payload: { results } })
    } catch (error) {

      dispatch({ type: 'SEARCH_FAILED', payload: { error: error.message } })
    }
  }

  return (
    <div>
      {/* .row*2>.col */}

      <div className="row">
        <div className="col">
          <SearchBox onSearch={search} />
        </div>
      </div>

      {error && <p className="alert alert-danger">Error: {error}</p>}

      {loading ? <p className="alert alert-info">Please wait... Loading</p> : <div className="row">
        <div className="col">
          <SearchResults results={results} />
        </div>
      </div>
      }
    </div>
  )
}
