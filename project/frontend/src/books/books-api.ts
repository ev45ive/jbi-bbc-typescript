import { Book } from "./model/Book"
import { BookItem, BookSearchResponse } from "./model/BookSearch"

const booksApiKey = 'AIzaSyCMbicbDiZyHWpRMr0r2qHqmvQ9wApulCo'

export const searchBooks = async (query: string): Promise<Book[]> => {

  const res = await fetch(`https://www.googleapis.com/books/v1/volumes?q=${query}&maxResults=40&key=${booksApiKey}`)
  const data = await res.json()

  if (!isVolumesResponse(data)) {
    throw new Error('Bad response')
  }
    
  const result = (data.items || []).map<Book>(item => ({
    id: item.id,
    name: item.volumeInfo.title,
    image: item.volumeInfo.imageLinks?.thumbnail || 'https://www.placecage.com/c/300/300'
  }))
  
  return result
}

function isVolumesResponse(data: any): data is BookSearchResponse {
  return (('kind' in (data) && data.kind === "books#volumes"))
}
