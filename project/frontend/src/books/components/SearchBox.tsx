import React, { useState } from 'react'

// tsrafc

interface Props {
  onSearch(query: string): void
}

export const SearchBox = ({ onSearch }: Props) => {
  const [query, setQuery] = useState('')

  return (
    <div>
      <div className="input-group mb-3">
        <input type="text" className="form-control" placeholder="Search books" value={query} onChange={e => setQuery(e.target.value)} />

        <button className="btn btn-outline-secondary" type="button" onClick={() => onSearch(query)}>Search</button>
      </div>
    </div>
  )
}
