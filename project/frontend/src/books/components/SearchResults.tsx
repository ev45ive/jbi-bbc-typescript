

import React, { FC } from 'react'
import { Book } from '../model/Book'

interface Props {
  results: Book[]
}

export const SearchResults = ({ results }: Props) => {
  return (
    <div className="row row-cols-1 row-cols-sm-4 g-0 no-gutters">
      {results.map(result => <div className="col" key={result.id}>
        <BookCard result={result} />
      </div>
      )}
    </div>
  )
}

const BookCard: FC<{ result?: Book }> = ({ result, children }) => {
  if (!result) return null

  return <div className="card">
    <img src={result.image} className="card-img-top" alt={result.name + ' image'} />
    <div className="card-body">
      <h5 className="card-title">{result.name}</h5>

    </div>
  </div>
}

BookCard.defaultProps = {
  result: {
    id: '123', name: '123', image: ''
  }
}

