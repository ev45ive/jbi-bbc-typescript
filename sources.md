

https://www.typescriptlang.org/docs/handbook/intro.html

https://basarat.gitbook.io/typescript/


## Full-Stack React, TypeScript, and Node
https://subscription.packtpub.com/book/web_development/9781839219931

## Learn TypeScript 3 by Building Web Applications
https://subscription.packtpub.com/book/programming/9781789615869

## Mastering TypeScript 3 - Third Edition
https://subscription.packtpub.com/book/application_development/9781789536706

## Advanced TypeScript Programming Projects
https://subscription.packtpub.com/book/application_development/9781789133042

## Refactoring TypeScript
https://subscription.packtpub.com/book/web_development/9781839218040

## Building Enterprise JavaScript Applications
https://subscription.packtpub.com/book/web_development/9781788477321

## Hands-On Functional Programming with TypeScript
https://subscription.packtpub.com/book/application_development/9781788831437

## Hands-On RESTful Web Services with TypeScript 3
https://subscription.packtpub.com/book/application_development/9781789956276

## TypeScript Microservices
https://subscription.packtpub.com/book/application_development/9781788830751

## Learn React with TypeScript 3
https://subscription.packtpub.com/book/web_development/9781789610253

## TypeScript High Performance
https://subscription.packtpub.com/book/application_development/9781785288647


https://www.udemy.com/course/understanding-typescript/


# Practice

https://developers.google.com/books/docs/v1/using#APIKey

https://www.googleapis.com/books/v1/volumes?q=isbn%3D9780134092669&key=AIzaSyCMbicbDiZyHWpRMr0r2qHqmvQ9wApulCo

https://www.googleapis.com/books/v1/volumes?q=${busqueda}&maxResults=40
https://www.googleapis.com/books/v1/volumes?q= +isbn:${busqueda}
https://www.googleapis.com/books/v1/volumes?q= +inauthor:${busqueda}
https://www.googleapis.com/books/v1/volumes?q=subject:${genre}
https://www.googleapis.com/books/v1/volumes?q=inpublisher:${publisher}
```ts
await (await fetch('https://www.googleapis.com/books/v1/volumes?q=flowers+inauthor:keyes&key=AIzaSyCMbicbDiZyHWpRMr0r2qHqmvQ9wApulCo')).json()
```

## Movies
```ts
await (await fetch('https://api.themoviedb.org/3/search/movie?api_key=036575fed00442456ce9dbf4746b4db4&query=alice')).json()
```

https://developers.themoviedb.org/3/getting-started/images
https://api.themoviedb.org/3/configuration?api_key=036575fed00442456ce9dbf4746b4db4&query=alice
https://api.themoviedb.org/3/movie/401123?api_key=036575fed00442456ce9dbf4746b4db4
<!-- https://image.tmdb.org/t/p/   + original/20pkC7yJdCV4J1IMKfsCT9QU7zV.jpg -->


## Spotify
http://developer.spotify.com/

```ts
var client_id = '224f8748135846bb8e4051449f2fe117';
var client_secret = '6ba921f038c34c2dbf62e5450042c32d';
await (await fetch('https://accounts.spotify.com/api/token',{
    method:'POST',
    headers:{
        'Content-Type':'application/x-www-form-urlencoded',
        'Authorization': `Basic ${btoa(`${client_id}:${client_secret}`)}`
    },
    body: new URLSearchParams({ grant_type: 'client_credentials' }).toString()

})).json()
```