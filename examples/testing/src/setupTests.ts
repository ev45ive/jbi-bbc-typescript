// testSetup.ts

// import '../index.d'

import chai from "chai";
import chaiAsPromised from "chai-as-promised";
import sinonChai from "sinon-chai"

chai.use(chaiAsPromised);
chai.use(sinonChai);

global.expect = chai.expect

declare global { var expect: Chai.ExpectStatic }