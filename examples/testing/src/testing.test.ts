
import { Test } from 'mocha';
import sinon, { SinonStub, SinonStubbedMember } from 'sinon'
// import promised from 'chai-as-promised'

// chai;
// ; ({}).should.be({})

describe('Testing test', () => {

  it('is testing', () => {
    expect(true).to.be.eq(true)
  });

  it('is async test', (done) => {
    setTimeout(() => {
      expect(true).not.to.be.false
      done()
    }, 0)
  })

  it('tests promises', async () => {
    const p = Promise.resolve(123)

    await expect(p).to.eventually.eq(123)
  })

  it('Test Mock', () => {
    const spy = sinon.spy()

    function hello(name: string, cb: (t: string) => void) {
      cb('hello ' + name)
    }

    hello('foo', spy)

    // spy.getCalls()[0].args
    // spy.getCalls()[0].returnValue
    expect(spy).to.have.been.calledWith('hello foo')
  })


  it('Mocks Test', () => {
    class Testing { someMethod(x: string) { return x } }

    // Arrange
    // const stub1 = sinon.stub(new Testing())
    // const stub2 = sinon.stub(new Testing(), 'someMethod')

    const stub = sinon.createStubInstance(Testing)
    stub.someMethod.returns('test')

    // Act
    stub.someMethod('test')

    // Assert
    expect(stub.someMethod).calledWith('test').and.returned('test')
  })

  class IblClient { async getEpisodes(id: string, options?: {}) { return [1] } }

  let iblClient: IblClient
  let getEpisodesMock: SinonStubbedMember<IblClient['getEpisodes']>;

  beforeEach(() => {
    iblClient = new IblClient()
    const sandbox = sinon.createSandbox();

    getEpisodesMock = sandbox.stub(iblClient, 'getEpisodes');
  })

  it('test', () => {

    getEpisodesMock.resolves([123]);

    iblClient.getEpisodes('123', {})

    // type funcParams = Parameters<typeof iblClient.getEpisodes>;
    // type funcReturn = ReturnType<typeof iblClient.getEpisodes>;

    // ; (iblClient.getEpisodes as SinonStub<funcParams,funcReturn>).resolves()

  })

  describe('Nested', () => {

    class IblClient { async getEpisodes(id: string, options?: {}) { return [1] } }

    const setUp = () => {
      const iblClient = new IblClient()
      const sandbox = sinon.createSandbox();
      const getEpisodesMock = sandbox.stub(iblClient, 'getEpisodes');
      return { iblClient, sandbox, getEpisodesMock }
    }
    // let ctx: ReturnType<typeof setUp>

    // beforeEach(() => ctx = setUp())

    it('', () => {
      const { getEpisodesMock, iblClient, sandbox } = setUp()
    })

  });

});

