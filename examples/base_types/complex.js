// https://developers.google.com/books/docs/v1/reference/volumes
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function (t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
;
var books = [
    {
        id: '123',
        isbn: '12345',
        name: 'TypeScript',
        pageCount: 352,
        authors: ['Bob'],
        categories: ['it'],
        pdf: { isAvailable: true }
    },
    {
        id: '234',
        isbn: '123456',
        name: 'JavaScript',
        pageCount: 234,
        authors: ['Alice'],
        categories: ['it', 'javascript'],
        pdf: { isAvailable: false }
    },
    {
        id: '567',
        isbn: '1234567',
        name: 'JavaScript and React',
        pageCount: 234,
        authors: ['Alice'],
        categories: ['it', 'javascript', 'react'],
        pdf: { isAvailable: false }
    },
];
var findBookById = function (id) { return books.find(function (book) { return book.id === id; }); };
// const findBookById: (id: BookData['id']) =>  'BookData' | undefined = (id) => { return books.find(book => book.id === id) }
// const findBookById = (id: BookData['id']): BookData | undefined => { return books.find(book => book.id === id) }
var findAllBooks = function () { return books; };
var getBookTagline = function (_a) {
    var name = _a.name, author = _a.authors[0];
    return name + " - " + author;
};
var findBooksByName = function (query) {
    return books.filter(function (book) { return book.name.toLocaleLowerCase().includes(query.toLocaleLowerCase()); });
};
var findBooksByIsbn = function (query) {
    return books.filter(function (book) { return book.isbn == query; });
};
var findBooksByGenre = function (genre) { return books.filter(function (book) { return book.categories.includes(genre); }); };
function findBooks(predicate) {
    if (predicate.name) {
        return findBooksByName(predicate.name);
    }
    else if (predicate.id) {
        return findBookById(predicate.id);
    }
    else if (predicate.isbn) {
        return findBooksByIsbn(predicate.isbn);
    }
    else if (predicate.genre) {
        return findBooksByGenre(predicate.genre);
    }
    else {
        throw new Error('Bad predicate');
    }
}
var searchResult = findBooks({ id: '123' });
var searchResult2 = findBooks({ name: '123' });
var createBook = function (bookPayload) {
    var bookDefaults = {
        isbn: '',
        pageCount: 0,
        authors: ['Unknown'],
        categories: [],
        pdf: { isAvailable: false }
    };
    var book = __assign(__assign({ id: parseInt((Math.random() * 10000).toString()).toString() }, bookDefaults), bookPayload);
    books.push(book);
    return book;
};
var updateBook = function (bookPayload) {
    var bookIndex = books.findIndex(function (b) { return b.id === bookPayload.id; });
    if (bookIndex !== -1) {
        books[bookIndex] = __assign(__assign({}, books[bookIndex]), bookPayload);
    }
};
var deleteBook = function (id) {
    var bookIndex = books.findIndex(function (b) { return b.id === id; });
    if (bookIndex !== -1) {
        delete books[bookIndex];
    }
};
var createBooks = function () {
    var books = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        books[_i] = arguments[_i];
    }
    books.forEach(createBook);
};
var xyz = {
    name: 'New Book',
    extra: 1
};
createBooks(xyz, xyz, { name: 'Last book' });
// const key:'name' = 'name';
// let key2: 'name' = 'name'
// let key2 = 'name' as const
// key2 = 'name2' // error 
var key = 'name';
key = 'isbn';

var DIR;

(function (DIR) {
    DIR[DIR["ASC"] = 1] = "ASC";
    DIR[DIR["DESC"] = -1] = "DESC";
    DIR[DIR["DEFAULT"] = 1] = "DEFAULT";
})(DIR || (DIR = {}));
var KEYS;

(function (KEYS) {
    KEYS["NAME"] = "name";
    KEYS["ISBN"] = "isbn";
    KEYS["PAGECOUNT"] = "pageCount";
})(KEYS || (KEYS = {}));

var bookSorter = {
    // dir: 'asc' as 'asc' | 'desc',
    dir: DIR.ASC,
    // key: 'name' as 'name' | 'isbn',
    key: KEYS.NAME,
    getComparator: function () {
        return function (a, b) {
            if (this.dir === DIR.DESC) {
                return a[this.key] > b[this.key] ? -1 : 1;
            }
            else {
                return a[this.key] > b[this.key] ? 1 : -1;
            }
        }.bind(this);
    }
};
bookSorter.dir = DIR.DESC;
// bookSorter.key = KEYS.PAGECOUNT; 
const uiSelectedKey: unknown = 'name'
bookSorter.key = KEYS.PAGECOUNT;
books.sort(bookSorter.getComparator());
console.log(books.map(getBookTagline));
