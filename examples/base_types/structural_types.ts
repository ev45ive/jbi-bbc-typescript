export {}

interface Request {
  url: string
  params: {}
}

interface Request {
  query: {},
  extra: object
}

const request: Request = {
  url: '',
  extra: {},
  params: {},
  query: ''
}


// ===

interface Point {
  x: number,
  y: number,
}

interface Vector {
  x: number,
  y: number,
  length: number
}

let p: Point = { x: 123, y: 123 }
let v: Vector = { x: 123, y: 123, length: 1000 }

p = v; // OK
// v = p; // Error



interface Track { }

interface Playlist {
  id: string
  name: string
  // tracks: Array<Track>
  tracks?: Track[],
}

let playlist: Playlist = { id: '123', name: 'Test' }

// Type Guard
if (playlist.tracks) {
  // (playlist.tracks as Track[]).length
  playlist.tracks.length
} else {
  playlist.tracks
}
let len1 = playlist.tracks ? playlist.tracks.length : 0
let len2 = playlist.tracks && playlist.tracks.length || 0

// NON-NULL ASSERION
let len3 = playlist.tracks?.length
let len4 = playlist.tracks!.length

// NULLISH COALESCING
let len5 = playlist.tracks?.length ?? 0 
