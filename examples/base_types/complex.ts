

// https://developers.google.com/books/docs/v1/reference/volumes

// type BookData = {
interface BookData extends EntityWithId {
  isbn: string,
  name: string;
  pageCount: number;
  authors: string[];
  categories: Array<string>;
  pdf: {
    isAvailable: boolean;
  }
};

interface CreateBookPayload {
  name: string;
  isbn?: string,
  pageCount?: number;
  authors?: string[];
}

interface EntityWithId { id: string }
// interface UpdateBookPayload extends EntityWithId, CreateBookPayload { }

// type UpdateBookPayload = { id: number } & CreateBookPayload;
type UpdateBookPayload = EntityWithId & CreateBookPayload;

const books: BookData[] = [
  {
    id: '123',
    isbn: '12345',
    name: 'TypeScript',
    pageCount: 352,
    authors: ['Bob'],
    categories: ['it'],
    pdf: { isAvailable: true }
  },
  {
    id: '234',
    isbn: '123456',
    name: 'JavaScript',
    pageCount: 234,
    authors: ['Alice'],
    categories: ['it', 'javascript'],
    pdf: { isAvailable: false }
  },
  {
    id: '567',
    isbn: '1234567',
    name: 'JavaScript and React',
    pageCount: 234,
    authors: ['Alice'],
    categories: ['it', 'javascript', 'react'],
    pdf: { isAvailable: false }
  },
]

interface FindByIdFn1 {
  // (): undefined;
  (id: BookData['id']): BookData | undefined;
}
type FindByIdFn2 = (id: BookData['id']) => BookData | undefined;

type FindByIdFn3 = ((id: BookData['id']) => BookData) | (() => undefined);

const findBookById: FindByIdFn1 = (id) => { return books.find(book => book.id === id) }
// const findBookById: (id: BookData['id']) =>  'BookData' | undefined = (id) => { return books.find(book => book.id === id) }
// const findBookById = (id: BookData['id']): BookData | undefined => { return books.find(book => book.id === id) }

const findAllBooks = (): BookData[] => { return books }

const getBookTagline = ({ name, authors: [author] }: BookData): string => { return `${name} - ${author}` }

const findBooksByName = (query: BookData['name']): BookData[] => {
  return books.filter(book => book.name.toLocaleLowerCase().includes(query.toLocaleLowerCase()))
}
const findBooksByIsbn = (query: BookData['isbn']): BookData[] => {
  return books.filter(book => book.isbn == query)
}

const findBooksByGenre = (genre: BookData['categories'][0]): BookData[] => { return books.filter(book => book.categories.includes(genre)) }


// interface findBooksFn {
// (predicate: { id: string }): BookData | undefined
// (predicate: { name: string }): BookData[]
// (predicate: { isbn: string }): BookData[]
//   (predicate: { [key: string]: string }): BookData | BookData[] | undefined
// }
// const findBooks: findBooksFn = (predicate: { [key: string]: string }) => {


type keys = 'id' | 'name' | 'isbn' | 'genre';
type predicate2 = { [key in keys]: string }

type IdPredicate = { id: string }
type NamePredicate = { name: string }
type IsbnPredicate = { isbn: string, exact?: boolean }
type GenrePredicate = { genre: string;};
type ArtistPredicate = { artist: string;};

type predicate =
  | IdPredicate
  | NamePredicate
  | IsbnPredicate 
  | GenrePredicate
  // | ArtistPredicate


function findBooks(predicate: { id: string }): BookData | undefined
function findBooks(predicate: { name: string }): BookData[]
function findBooks(predicate: { isbn: string }): BookData[]
function findBooks(predicate: { genre: string }): BookData[]
function findBooks(predicate: predicate): BookData | BookData[] | undefined { // implmentation signature
  // function findBooks(predicate: { [key in 'name' | 'id' | 'isbn' | 'genre']: string }): BookData | BookData[] | undefined {
    // predicate.name // Error
  if ('name' in predicate) {
    return findBooksByName(predicate.name)
  } else if ('id' in predicate) {
    return findBookById(predicate.id)
  } else if ('isbn' in predicate) {
    return findBooksByIsbn(predicate.isbn)
  } else if ('genre' in predicate) {
    return findBooksByGenre(predicate.genre)
  } else {
    const _never: never = predicate;
    // checkExhaustiveness(predicate);
  }

  function checkExhaustiveness(_never: never): never {
    throw new Error('Bad predicate');
  }
}

const searchResult = findBooks({ id: '123' });
const searchResult2 = findBooks({ name: '123' });


const createBook = (bookPayload: CreateBookPayload) => {
  const bookDefaults = {
    isbn: '',
    pageCount: 0,
    authors: ['Unknown'],
    categories: [],
    pdf: { isAvailable: false }
  }
  const book: BookData = {
    id: parseInt((Math.random() * 10_000).toString()).toString(),
    ...bookDefaults,
    ...bookPayload,
  }
  books.push(book)
  return book
}
const updateBook = (bookPayload: UpdateBookPayload) => {
  const bookIndex = books.findIndex(b => b.id === bookPayload.id)
  if (bookIndex !== -1) {
    books[bookIndex] = {
      ...books[bookIndex],
      ...bookPayload
    }
  }
}

const deleteBook = (id: BookData['id']) => {
  const bookIndex = books.findIndex(b => b.id === id)
  if (bookIndex !== -1) {
    delete books[bookIndex]
  }
}

const createBooks = (...books: CreateBookPayload[]) => {
  books.forEach(createBook)
}

const xyz = {
  name: 'New Book',
  extra: 1
}
createBooks(xyz, xyz, { name: 'Last book' })


// const key:'name' = 'name';
// let key2: 'name' = 'name'
// let key2 = 'name' as const
// key2 = 'name2' // error 

let key: 'name' | 'isbn' = 'name';
key = 'isbn'

enum DIR {
  ASC = 1, DESC = -1, DEFAULT = DIR.ASC
}

enum KEYS {
  NAME = 'name',
  ISBN = 'isbn',
  PAGECOUNT = 'pageCount',
}

const bookSorter = {
  // dir: 'asc' as 'asc' | 'desc',
  dir: DIR.ASC,
  // key: 'name' as 'name' | 'isbn',
  key: KEYS.NAME,
  getComparator() {
    return function (this: typeof bookSorter, a: BookData, b: BookData) {
      if (this.dir === DIR.DESC) {
        return a[this.key] > b[this.key] ? -1 : 1;
      } else {
        return a[this.key] > b[this.key] ? 1 : -1;
      }
    }.bind(this)
  }
}
bookSorter.dir = DIR.DESC;
// bookSorter.key = KEYS.PAGECOUNT; 
const uiSelectedKey: string = 'name'
bookSorter.key = KEYS.PAGECOUNT
// bookSorter.key = KEYS[uiSelectedKey]
books.sort(bookSorter.getComparator())



console.log(books.map(getBookTagline)) 


type Headers = "Accept" | "Authorization" /* … */;
type RequestConfig = {
  [header in Headers]: string;
};
type RequestConfigSnapshot = {
  readonly [header in Headers]: string;
};

const requestConfig: RequestConfigSnapshot = {
  Accept: "…",
  Authorization: '123',
  // extra:'test'
};

// requestConfig.Authorization = '123'