
// let isThisBookOkay: boolean = true
let isThisBookOkay = true
// let isThisBookOkay:true = true
// let isThisBookOkay = true as const

isThisBookOkay = true
isThisBookOkay = false


Number(123)
// 123
Number('123')
// 123
typeof Number('123')
// "number"
typeof new Number('123')
// "object"
new Number('123')
// Number {123}__proto__: Number[[PrimitiveValue]]: 123

// typeof 10n
// "bigint"
// typeof 10
// "number"

// const result: number = 123 + 0xbeef + 0b1111 + 0o700;
// const result:bigint = 123 + 0xbeef + 0b1111 + 0o700;
const result = 123 + 0xbeef + 0b1111 + 0o700;
// 49465

const res2: number = Number(123)

const firstName: string = 'Mateusz';
const lastName: string = "Kulesza";
const hello: string = `Hello,
${firstName} ${lastName}!`;
// Hello,\nMateusz Kulesza!

function getValue(key: string): any { };

// OK, return value of 'getValue' is not checked
const str: number = getValue("myString");
const no: string = getValue("myString");

// Any propagates down to members:
let looselyTyped: any = {};
let d = looselyTyped.a.b.c.d.getMeAmillionDollars();


function ireturnvoid(): void {  /* return 123 */ }
const iamvoid = ireturnvoid()

// Function returning never must not have a reachable end point
function makeError(message: string): never {
  // if (message.length > 3) { return message }
  throw new Error(message);
  // return undefined
}

const neverarray: never[] = []
// neverarray.push(123)

// Inferred return type is never
function fail() {
  makeError("Something failed");
  return true
}

const x = fail()


// Function returning never must not have a reachable end point
function infiniteLoop(): never { while (true) { } }

function isObject(obj: object | null) {
  if (obj !== null) {/* ... */ }
}
isObject(null)


// Way 1: Type[] 
let list1: number[] = [1, 2, 3];
// The second way uses a generic array type, Array<elemType>:

let list3: { name: string }[] = [{ name: '123' }]

// Way 2: Array<Type>
let list2: Array<number> = [1, 2, 3];

let list4 = [1, 2, 3, 'test'];

let list5: Array<{ name: string }> = [{ name: '123' }];

// let notAList: [number] = [1,2,3] // Error

// let personTuple: [number,string] = [123, 'Alice']
let personTuple: [id: number, name: string] = [123, 'Alice']
personTuple = [123, 'Bob']
personTuple = [123, 'Typescript Book']

let notTuple = personTuple.slice()
notTuple = [1, 2, 3];

let [id, personName] = personTuple


function random() {
  return Math.random() > 0.5 ? 123 : 'test'
}