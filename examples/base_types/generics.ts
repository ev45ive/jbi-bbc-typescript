export { }

interface Constructable<T> {
  new(...args: any[]): T;
}

class Person { }

const pClasss: typeof Person = Person;
const pInst: Person = new Person();
const pInst2: Person = new pClasss();


const builder1: Constructable<typeof pInst> = Person
const builder2: Constructable<Person> = Person


function echo<T extends string>(msg: T): T {
  return msg
}
echo('123')
echo(String('123'))
const x: 'A' | 'B' | 'C' = 'C';
echo(x)
function getRoleByKey<T = "Admin" | "User">(key: T) { return key }
const res = getRoleByKey('User')

interface User { id: string }
interface Admin /* extends User */ { id: string, role: 'Moderator' }
interface Moderator /* extends User */ { id: string, role: 'Moderator' }

function addUser<T extends User>(u: T) { }
addUser({} as Admin)
addUser({} as Moderator)


function defaults<T extends object, U extends T>(obj1: T, obj2: U) {
  return { ...obj1, ...obj2 };
}
defaults({ a: '' }, { a: '1', b: 2 })


function merge<T, U>(a: T, b: U): T & U {
  return { ...a, ...b }
}
const merged = merge({ a: 1 }, { b: 2 })
merged.a
merged.b
type keys = keyof typeof merged;

function getKey<T extends object, U extends keyof T>(obj: T, key: U) {
  return obj[key]
}
getKey({ a: 1, b: 2 }, 'b')

/*
  T = ?
  T extends ?
  T extend keyof ?
*/