export { }

enum BookSource {
  UNKNOWN = 'UNKNOWN',
  GOOGLE_BOOKS = 'GOOGLE_BOOKS',
  AMAZON = 'AMAZON',
  OPENBOOKAPI = 'OPENBOOKAPI'
}

interface EntityWithId { id?: string }

interface BookData extends EntityWithId {
  readonly type: 'book'
  isbn: string,
  name: string;
  pageCount: number;
  authors: string[];
  categories: Array<string>;
  pdf: {
    isAvailable: boolean;
  }
  source?: BookSource
};

class MediaEntity implements EntityWithId {
  constructor(
    readonly id: string,
    public name: string
  ) { }

  protected sourceInfo = {
    source: BookSource.UNKNOWN,
    sourceId: '',
    pdfIsAvailable: false
  }
}

class Book extends MediaEntity {
  readonly type = "book"

  isbn = ''
  pageCount = 0
  authors = []
  categories = []

  updateSource(source: BookSource, sourceId: string) {
    this.sourceInfo.source = source
    this.sourceInfo.sourceId = sourceId
  }

  /* private */ constructor(id: string, name: string) {
    super(id, name)
  }

  // toJSON(): BookData { return { } } // book.fromJSON()

  static fromJSON(data: BookData): Book // Book.fromJSON()
  // static fromJSON(data: any): Book 
  {
    if (!data.id) { throw new Error('Missing ID') }

    const book = new Book(data.id, data.name)
    book.isbn = data.isbn
    if (data.source) {
      book.sourceInfo.source = data.source
      book.sourceInfo.sourceId = data.id
    }
    return book
  }

  static getSource(anotherBook: Book) {
    return anotherBook.sourceInfo.source
  }

}

class BookRepository {
  _items: Book[] = []

  find() { }
  findOne() { }

  create() { }
}

const book = new Book('123', 'New book')
book.id


