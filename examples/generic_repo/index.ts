export { }

interface MediaItem { id: string, name: string }

enum Quality { }

interface Book extends MediaItem { isbn: string, pagesCount: number }
interface BookPayload { id?: Book['id'], name?: Book['name'], isbn?: Book['isbn'] }

type BookKeys = keyof Book

interface ArrayLike<T> { [key: string]: T | number, length: number }

// type BookLike = { [k in keyof BookPayload]-? : BookPayload[k] }
// type BookLike = { [k in keyof Book] : Book[k] }
// type PartialBook = { [k in keyof Book]? : Book[k] }

// type Partial<T> = { [key in keyof T]?: T[key] }
// C:\Users\%USER%\AppData\Local\Programs\Microsoft VS Code\resources\app\extensions\node_modules\typescript\lib\lib.es5.d.ts
// https://github.com/pelotom/type-zoo
// https://gcanti.github.io/typelevel-ts/modules/index.ts.html#deepreadonlyarray-interface

type PartialBook = Partial<Book>

type Pick<T, K extends keyof T> = { [key in K]: T[key] }

type SimpleBook = Pick<Book, 'id' | 'name' | 'pagesCount' /* | 'pancakes' */>

type PartialSimpleBook = Partial<Pick<Book, 'name'>> & { id: Book['id'] }

type OnlyString<T> = T extends string ? T : never

type OnlyStringKeys<T> = {
  [K in keyof T]-? : T[K] extends string? K : never;
}[keyof T]

type KeysMatching<T, V> = {
  [K in keyof T]-?: T[K] extends V ? K : never
}[keyof T];

type onlyStr = Pick<Book, KeysMatching<Book, string>>

type o = OnlyStringKeys<Book>

type BookStringKeys = OnlyStringKeys<Book>

type FirstAndLastName<T = any> = T extends { name: string } ? T & { lastname: string } : T;

type OnlyName = { name: string }
type WithoutName = { lastname: string }


let firstLast: FirstAndLastName<OnlyName | WithoutName> = {
  name: '123', lastname: '123'
}

type Exclude<T, U> = T extends U ? never : T;

// type BookWithoutId = Pick<Book, Exclude<keyof Book, 'id'>>
type Omit<T, K> = Pick<T, Exclude<keyof T, K>>
type BookWithoutId = Omit<Book, 'id'>

class BookCriteria implements Criteria<Book> {
  constructor(readonly payload: { name: string }) { }

  match = (item: Book): boolean => {
    return item.name.toLocaleLowerCase().includes(this.payload.name.toLocaleLowerCase())
  }
}

interface Movie extends MediaItem { duration: number, quality: Quality }
interface Album extends MediaItem { artists: string[] }

interface Criteria<T extends MediaItem> {
  match(item: T): boolean
}

interface GenericRepository<T extends MediaItem, P = {}> {

  find(criteria: Criteria<T>): T[]
  findOne(criteria: Criteria<T>): T | undefined
  findOneById(id: T['id']): T | undefined

  create(payload: P): T
  remove(item: T['id']): boolean

  // Insert of Update
  save(item: T | P): T
}

// TODO:
// - make interface generic
// - craete class implementing generic with concrete type

class BookRepository implements GenericRepository<Book> {
  items: Book[] = []

  find<C extends Criteria<Book>>(criteria: C): Book[] {
    return this.items.filter(criteria.match)
  }

  findOne<C extends Criteria<Book>>(criteria: C): Book | undefined {
    return this.items.find(criteria.match)
  }

  findOneById(id: string): Book | undefined {
    return this.items.find(b => b.id === id)
  }

  create(payload: Omit<Partial<Book>, 'id'>): Book {
    const defaults = { name: '', isbn: '', pagesCount: 0 }
    const book: Book = {
      id: parseInt((Math.random() * 10_000).toString()).toString(),
      ...defaults,
      ...payload
    }
    this.items.push(book)
    return book
  }

  remove(itemId: Book['id']): boolean {
    const index = this.items.findIndex(b => b.id == itemId)
    if (index === -1) { return false }
    this.items.splice(index, 1)
    return true;
  }

  save(item: Book): Book {
    const index = this.items.findIndex(b => b.id == item.id)
    if (index === -1) { throw new Error('Book does not exist, cannot update') }
    this.items.splice(index, 1, item)
    return item;
  }

}

const booksRepo = new BookRepository()
booksRepo.create({ name: 'Alice in wonderland' })
const book = booksRepo.create({ name: 'Alice and bob' })
book.name = 'Resident Evil: Alice'

booksRepo.save(book)
booksRepo.create({ name: 'Lord of the rings' })
booksRepo.create({ name: 'Expanse' })
const result = booksRepo.find(new BookCriteria({ name: 'Alice' }))
console.log(result)


// ;[1,2,3].map()