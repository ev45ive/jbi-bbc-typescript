function Person(name){

  this.name = name;

  this.sayHello = sayHello
}
function sayHello(){
    return "Hi, I am " + this.name
}
var alice = new Person('Alice');
var bob = new Person('Bob');

// -----

const obj2 = Object.create({
    base: 'base'
})
obj2.base 
"base"

obj2.base = 'override'
"override"

obj2.base 
"override"
obj2
// {base: "override"}base: "override"__proto__: base: "base"__proto__: Object
delete obj2.base 
obj2
// {}__proto__: base: "base"__proto__: Object
 obj2.base 
"base"

 // ---

 function Person(name){
  this.name = name;
}
Person.prototype.sayHello = function() {
    return "Hi, I am " + this.name
}
// new => Object.create(Person.prototype) .apply(Person, [name] )

var alice = new Person('Alice');
var bob = new Person('Bob');
alice.sayHello === bob.sayHello
// true
bob.sayHello = function(){ return 'I dont talke with you!' }
// ƒ (){ return 'I dont talke with you!' }
bob.sayHello()
// "I dont talke with you!"
alice.sayHello()
// "Hi, I am Alice"

// ---

function Employee(name,salary){
  Person.apply(this,[name])
  this.salary = salary
}
Employee.prototype = Object.create(Person.prototype); 
Employee.prototype.work = function(){ 
  return 'Can I have my ' + this.salary;
}

tom = new Employee('Tom', 2500)
console.log(tom.sayHello())
tom.work() 

// VM3602:11 Hi, I am Tom
"Can I have my 2500"
tom 
// Employee {name: "Tom", salary: 2500}
// name: "Tom"
// salary: 2500
// __proto__: Person
// work: ƒ ()
// __proto__:
// sayHello: ƒ ()
// constructor: ƒ Person(name)
// __proto__: Object

// --- 

div = document.createElement('div')


{/* <div>​</div>​
div.id = 123
123
div.__proto__
HTMLDivElement {Symbol(Symbol.toStringTag): "HTMLDivElement", constructor: ƒ}
div.__proto__.__proto__
HTMLElement {…}
div.__proto__.__proto__.__proto__
Element {…}
div.__proto__.__proto__.__proto__.__proto__
Node {…}
div.__proto__.__proto__.__proto__.__proto__.__proto__
EventTarget {Symbol(Symbol.toStringTag): "EventTarget", addEventListener: ƒ, dispatchEvent: ƒ, removeEventListener: ƒ, constructor: ƒ}addEventListener: ƒ addEventListener()dispatchEvent: ƒ dispatchEvent()removeEventListener: ƒ removeEventListener()constructor: ƒ EventTarget()Symbol(Symbol.toStringTag): "EventTarget"__proto__: Object
div.__proto__.__proto__.__proto__.__proto__.__proto__.__proto__
{constructor: ƒ, __defineGetter__: ƒ, __defineSetter__: ƒ, hasOwnProperty: ƒ, __lookupGetter__: ƒ, …}
div.__proto__.__proto__.__proto__.__proto__.__proto__.__proto__.__proto__
null */}