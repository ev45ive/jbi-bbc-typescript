function echo(msg, cb){

    setTimeout(()=>{
         cb(msg)
    },2000)
}

res = echo('Hello', res => {
    echo(res + ' Callbacks', res2 => {
        console.log(res2)
    })
})

// ====


function echo(msg){
  return new Promise((resolve)=>{
      setTimeout(()=>{
           resolve(msg)
      },2000)
  });
}

res = echo('Hello');

res.then(res => {
  echo(res + ' Callbacks',).then(res2 => {
      console.log(res2)
  })
})

res = echo('Hello');

res.then(res => echo(res + ' Callbacks'))
   .then(res2 => console.log(res2) )


res.then(res => echo(res + ' Promises'))
.then(res2 => console.log(res2) )

// ===

function echo(msg, error){
  return new Promise((resolve, reject)=>{
      setTimeout(()=>{
           error? reject(error) : resolve(msg)
      },2000)
  });
}

res = echo('Hello', 'Upss..');

res.then(res => echo(res + ' Callbacks'), error => ' Error in ')
 .then(res2 => console.log(res2) )


res.then(res => echo(res + ' Promises'))
 .then(res2 => console.log(res2) )
  .catch(error => console.log('Error somewhere before'))

// ===

function echo(msg, error){
  return new Promise((resolve, reject)=>{
      setTimeout(()=>{
           error? reject(error) : resolve(msg)
      },2000)
  });
}

try{
res = await echo('Hello' /*, 'Upss..' */);

res2 = await echo(res + ' Async Await')
  
console.log( res2) 

}catch(error){
  console.log('error '+error)
}
