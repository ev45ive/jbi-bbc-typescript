function sum(items){
    return items.reduce((sum,x)=>sum+x,0)
}
undefined
sum([1,3,4])
8
function sum(...items){
    return items.reduce((sum,x)=>sum+x,0)
}
undefined
sum(1,3,4)
8
function sum(first = 0, ...items){
    return items.reduce((sum,x)=>sum+x,first)
}
undefined
sum(1,3,4)
8
sum(...[1,3,4])
8