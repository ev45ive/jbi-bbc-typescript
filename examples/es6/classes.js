class Person{
  name = ''
  age = 18

  callback = () => { return this; }
   
 
  constructor(name){
    this.name = name;
  }

  sayHello(){ return 'Hello, Iam ' + this.name }
}
undefined
alice = new Person('Alice')
Person {name: "Alice", age: 18, callback: ƒ}
fn = alice.callback
() => { return this; }
fn() 
Person {name: "Alice", age: 18, callback: ƒ}
fn2 = alice.sayHello
ƒ sayHello(){ return 'Hello, Iam ' + this.name }
fn2()
VM11265:12 Uncaught TypeError: Cannot read property 'name' of undefined
    at sayHello (<anonymous>:12:43)
    at <anonymous>:1:1

// ---
    
function Employee(
    name,
    salary = Employee.MINIMAL_SALARY = 1000
){
   
}
Employee.MINIMAL_SALARY = 1000;

tom = new Employee('Tom', 2500)

// ---

class Person{
  name = ''
  age = Person.MINIMUM_AGE

  callback = () => { return this; }
   
  static MINIMUM_AGE = 18
 
  constructor(name){
    this.name = name;
  }

  sayHello(){ return 'Hello, Iam ' + this.name }
}
alice = new Person('Alice')
Person {name: "Alice", age: 18, callback: ƒ}age: 18callback: () => { return this; }name: "Alice"__proto__: Object
Person.MINIMUM_AGE
18


class Person{
  name = ''
  age = Person.MINIMUM_AGE

  callback = () => { return this; }
   
  static MINIMUM_AGE = 18
 
  constructor(name){
    this.name = name;
  }

  sayHello(){ return 'Hello, Iam ' + this.name }
}
alice = new Person('Alice')

class Employee extends Person{

  constructor(name,salary){
    super(name)
    this.salary = salary;
  }

  work(){ return this.salary }

  sayHello(){
    return super.sayHello() + ' I am an Employee!'
  }
}
Person {name: "Alice", age: 18, callback: ƒ}
tom = new Employee('Tom', 2500)
tom.work() 
2500
tom.sayHello()
"Hello, Iam Tom I am an Employee!"
tom
// Employee {name: "Tom", age: 18, salary: 2500, callback: ƒ}
// age: 18
// callback: () => { return this; }
// name: "Tom"
// salary: 2500
  // __proto__: Person
  // constructor: class Employee
  // sayHello: ƒ sayHello()
  // work: ƒ work()
    // __proto__:
    // constructor: class Person
    // sayHello: ƒ sayHello()
      // __proto__: Object