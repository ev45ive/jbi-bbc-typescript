// var list = [1,2,3]
// a = list[0],
// b = list[2]

var [a, , b] = [1,2,3];



var person = {
    name:'Alice',
    address:{ street: 'HighSt.', city:'London' },
    company:{ name: 'ACME' },
    roles:['admin','moderator']
};

var {name:personName} = person
var {name} = person

var { 
    name, 
    address: { street, city }, 
    company: { name: companyName }, 
    roles: [firstRole] 
} = person;

`${name} [${companyName}] - ${street} ${city} - {${firstRole}}`


/// ----

var person = {
    name:'Alice',
    address:{ street: 'HighSt.', city:'London' },
//     company:{ name: 'ACME' },
    roles:['admin','moderator']
};

var { 
    name = 'Anonymous', 
    address: { street, city }, 
    company: { name: companyName } = {name:'Unemployed'}, 
    roles: [firstRole] 
} = person;

`${name} [${companyName}] - ${street}. ${city} - {${firstRole}}`


/// ---

var person = {
    name:'Alice',
    address:{ street: 'HighSt.', city:'London' },
//     company:{ name: 'ACME' },
    roles:['admin','moderator']
};

getPersonInfo = (person) => {
    var { 
        name = 'Anonymous', 
        address: { street, city }, 
        company: { name: companyName } = {name:'Unemployed'}, 
        roles: [firstRole] 
    } = person;

    return `${name} [${companyName}] - ${street}. ${city} - {${firstRole}}`
}
getPersonInfo(person)


/// ---

var person = {
    name:'Alice',
    address:{ street: 'HighSt.', city:'London' },
//     company:{ name: 'ACME' },
    roles:['admin','moderator']
};

getPersonInfo = ({ 
    name = 'Anonymous', 
    address: { street, city }, 
    company: { name: companyName } = {name:'Unemployed'}, 
    roles: [firstRole] 
}) => ({
    name,
    tagline: `${name} [${companyName}] - ${street}. ${city} - {${firstRole}}`
})
getPersonInfo(person)