
function add(a?: string | null, b?: string | null) {
  return a && b && a + b;
}

add('1', '2')
add('1', '2')

add( undefined, undefined )
add( )
add( null, null )